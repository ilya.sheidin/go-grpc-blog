FROM golang:latest
# Arguments defined in docker-compose.yml

RUN apt-get update && \
    apt-get -y install git unzip build-essential autoconf libtool

WORKDIR /app
RUN git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    ldconfig && \
    make clean && \
    cd .. && \
    rm -r protobuf

# NOTE: for now, this docker image always builds the current HEAD version of
# gRPC.  After gRPC's beta release, the Dockerfile versions will be updated to
# build a specific version.

# Get the source from GitHub
##RUN go get google.golang.org/grpc
# Install protoc-gen-go
RUN go get -u github.com/golang/protobuf/protoc-gen-go


COPY ./git-grpc-blog-server /app
RUN rm -rf /go/pkgi
RUN rm -rf vendor
RUN go mod tidy
RUN go mod vendor
RUN go build -mod=vendor
RUN go get github.com/githubnemo/CompileDaemon

#ENTRYPOINT CompileDaemon --build="./generate.sh && go build server.go" --command=./server

