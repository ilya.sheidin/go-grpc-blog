FROM golang:latest
COPY ./go-grpc-client /app
WORKDIR /app
RUN rm -rf vendor
RUN go mod tidy
RUN go mod vendor
RUN go build -mod=vendor
RUN go get github.com/githubnemo/CompileDaemon

#ENTRYPOINT CompileDaemon --build="go build client.go" --command=./client